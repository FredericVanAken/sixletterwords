﻿namespace SixLetterWords;

class Program
{
    static void Main(string[] args)
    {
        Program program = new Program();
        program.Run();
    }

    private void Run()
    {
        List<string> input = FileIo.ReadInput("input.txt");
        List<string> wordParts = input.Where(x => x.Length < 6).ToList();
        List<string> completeWords = input.Where(x => x.Length == 6).ToList();
        List<CombinedWord> combinedWords = new List<CombinedWord>();
        foreach (string wordPart in wordParts)
        {
            CombinedWord combinedWord = new CombinedWord();
            combinedWord.AddWordToCombinedWord(completeWords, wordPart);
            WordsFinder.AddWordPart(wordParts, completeWords, combinedWord);
            if (!combinedWords.Any(c => c.ToStringOutput().Equals(combinedWord.ToStringOutput())))
            {
                combinedWords.Add(combinedWord);
            }
        }

        FileIo.WriteOutput(combinedWords);
    }
}