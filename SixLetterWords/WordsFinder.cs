﻿namespace SixLetterWords;

public static class WordsFinder
{
    public static void AddWordPart(List<string> wordParts, List<string> completeWords, CombinedWord combinedWord)
    {
        foreach (string wordPart in wordParts)
        {
            combinedWord.AddWordToCombinedWord(completeWords, wordPart);
        }
    }
}