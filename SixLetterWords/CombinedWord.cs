﻿namespace SixLetterWords;

public class CombinedWord
{
    public ICollection<string> CombiningWords { get; set; }

    public CombinedWord()
    {
        CombiningWords = new List<string>();
    }
    
    public void AddWordToCombinedWord(List<string> completeWords, string word)
    {
        string tmp = ToString() + word;
        if (tmp.Length <= 6 && completeWords.Any(s => s.IndexOf(tmp, 0, tmp.Length, StringComparison.Ordinal) == 0))
        {
            CombiningWords.Add(word);
        }
    }
    
    public string ToStringOutput()
    {
        return string.Join("+", CombiningWords) + "=" + ToString();;
    }

    public override string ToString()
    {
        return string.Concat(CombiningWords);
    }

    public override bool Equals(object? obj)
    {
        if (obj == null || GetType() != obj.GetType())
        {
            return false;
        }

        CombinedWord cw = (CombinedWord)obj;
        
        return ToString().Equals(cw.ToString());
    }

    public override int GetHashCode()
    {
        return base.GetHashCode();
    }
}