﻿namespace SixLetterWords;

public static class FileIo
{
    public static List<string> ReadInput(string path)
    {
        StreamReader streamReader = new StreamReader(path);
        List<string> list = new List<string>();
        try
        {
            var line = streamReader.ReadLine();
            while (line != null)
            {
                list.Add(line);
                line = streamReader.ReadLine();
            }

            streamReader.Close();
        }
        catch (Exception e)
        {
            Console.WriteLine("Exception: " + e.Message);
        }

        return list.ToList();
    }

    public static void WriteOutput(List<CombinedWord> combinedWords)
    {
        StreamWriter streamWriter = new StreamWriter("output.txt");
        try
        {
            foreach (CombinedWord combinedWord in combinedWords.OrderBy(cw => cw.ToString()))
            {
                streamWriter.WriteLine(string.Join("+", combinedWord.CombiningWords) + "=" + combinedWord);
            }

            streamWriter.Close();
        }
        catch (Exception e)
        {
            Console.WriteLine("Exception: " + e.Message);
        }
    }
}