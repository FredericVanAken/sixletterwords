using SixLetterWords;

namespace SixLetterWordsUnitTests;

[TestClass]
public class SixLetterWordsTests
{
    [TestMethod]
    public void AllCompleteWordsExist()
    {
        List<string> input = FileIo.ReadInput("../../../../input.txt");
        List<string> wordParts = input.Where(x => x.Length < 6).ToList();
        List<string> completeWords = input.Where(x => x.Length == 6).ToList();
        List<CombinedWord> combinedWords = new List<CombinedWord>();
        foreach (string wordPart in wordParts)
        {
            CombinedWord combinedWord = new CombinedWord();
            combinedWord.AddWordToCombinedWord(completeWords, wordPart);
            WordsFinder.AddWordPart(wordParts, completeWords, combinedWord);
            if (!combinedWords.Contains(combinedWord))
            {
                combinedWords.Add(combinedWord);
            }
        }

        List<string> allCombinedWords = new List<string>();
        combinedWords.Distinct().ToList()
                .ForEach(cw => allCombinedWords.Add(cw.ToString()));
        Assert.IsTrue(completeWords.Distinct().Count(s => allCombinedWords.Contains(s)) == allCombinedWords.Count, "Found words do not match combined words.");
    }
}